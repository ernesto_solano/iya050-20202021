const expect = require('chai').expect;

const request = {
  state: [
    {
      questions: [],
      answers: [],
      board: [{ name: 'Juan Perez', picture: 'juanperez.jpg', iswinner: true }],
      isStarter: 0,
      isWinner: 0,
    },
    {
      questions: [],
      answers: [],
      board: [{ name: 'Pedro Gonzalez', picture: 'pedrogonzalez.jpg', iswinner: false }],
      isStarter: 0,
      isWinner: 0,
    },
  ],
  question: 'is he bald?',
};

const invalidRequest = {
  state: [
    {
      questions: [],
      answers: [],
      board: [{ name: 'Juan Perez', picture: 'juanperez.jpg', iswinner: true }],
      isStarter: 0,
      isWinner: 0,
    },
    {
      questions: ['is your character young?'],
      answers: [],
      board: [{ name: 'Pedro Gonzalez', picture: 'pedrogonzalez.jpg', iswinner: false }],
      isStarter: 1,
      isWinner: 0,
    },
  ],
  question: 'is he bald?',
};

const response = [
  {
    questions: ['is he bald?'],
    answers: [],
    board: [
      {
        name: 'Juan Perez',
        picture: 'juanperez.jpg',
        iswinner: true,
      },
    ],
    isStarter: 1,
    isWinner: 0,
  },
  {
    questions: [],
    answers: [],
    board: [
      {
        name: 'Pedro Gonzalez',
        picture: 'pedrogonzalez.jpg',
        iswinner: false,
      },
    ],
    isStarter: 0,
    isWinner: 0,
  },
];

describe('Question API', function () {
  it('returns a JSON', function () {
    cy.request('POST', '/api/v1/question', request)
      .its('headers')
      .its('content-type')
      .should('include', 'application/json');
  });
  it('returns the expected JSON', function () {
    cy.request('POST', '/api/v1/question', request).its('body').should('deep.eq', response);
  });
  it('returns 400 error when invalid action', function () {
    cy.request({ method: 'POST', url: '/api/v1/question', failOnStatusCode: false, body: invalidRequest })
      .its('status')
      .should('deep.eq', 400);
  });
});
