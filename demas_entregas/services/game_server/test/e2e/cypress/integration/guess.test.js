const expect = require('chai').expect;

const request = {
  state: [
    {
      questions: ['is he blonde?'],
      answers: [],
      board: [{ name: 'Juan Perez', picture: 'juanperez.jpg', iswinner: true }],
      isStarter: 1,
      isWinner: 0,
    },
    {
      questions: [],
      answers: ['yes'],
      board: [{ name: 'Pedro Gonzalez', picture: 'pedrogonzalez.jpg', iswinner: false }],
      isStarter: 0,
      isWinner: 0,
    },
  ],
  guess: 'juan perez',
};
const invalidRequest = {
  state: [
    {
      questions: ['is he blonde?'],
      answers: [],
      board: [{ name: 'Juan Perez', picture: 'juanperez.jpg', iswinner: true }],
      isStarter: 1,
      isWinner: 0,
    },
    {
      questions: [],
      answers: [],
      board: [{ name: 'Pedro Gonzalez', picture: 'pedrogonzalez.jpg', iswinner: false }],
      isStarter: 0,
      isWinner: 0,
    },
  ],
  guess: 'juan perez',
};
const response = [
  {
    questions: ['is he blonde?'],
    answers: [],
    board: [
      {
        name: 'Juan Perez',
        picture: 'juanperez.jpg',
        iswinner: true,
      },
    ],
    isStarter: 1,
    isWinner: 1,
  },
  {
    questions: [],
    answers: ['yes'],
    board: [
      {
        name: 'Pedro Gonzalez',
        picture: 'pedrogonzalez.jpg',
        iswinner: false,
      },
    ],
    isStarter: 0,
    isWinner: 0,
  },
];

describe('Question API', function () {
  it('returns a JSON', function () {
    cy.request('POST', '/api/v1/guess', request)
      .its('headers')
      .its('content-type')
      .should('include', 'application/json');
  });
  it('returns the expected JSON', function () {
    cy.request('POST', '/api/v1/guess', request).its('body').should('deep.eq', response);
  });
  it('returns 400 error when invalid action', function () {
    cy.request({ method: 'POST', url: '/api/v1/guess', failOnStatusCode: false, body: invalidRequest })
      .its('status')
      .should('deep.eq', 400);
  });
});
