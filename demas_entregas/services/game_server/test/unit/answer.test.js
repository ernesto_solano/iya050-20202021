const assert = require('chai').assert;
const answer = require('../../src/controllers/answer.controller.js');

describe('Answer', function () {
  describe('#add()', function () {
    const game = [
      {
        questions: ['is he bald?'],
        answers: [],
        board: [{ name: 'Juan Perez', picture: 'juanperez.jpg', iswinner: false }],
        isStarter: 1,
        isWinner: 0,
      },
      {
        questions: [],
        answers: [],
        board: [{ name: 'Pedro Gonzalez', picture: 'pedrogonzalez.jpg', iswinner: false }],
        isStarter: 0,
        isWinner: 0,
      },
    ];

    it('should return a JSON of the game state with the answer added when the answer is valid', function () {
      assert.deepStrictEqual(answer.add('yes', game), [
        {
          questions: ['is he bald?'],
          answers: [],
          board: [{ name: 'Juan Perez', picture: 'juanperez.jpg', iswinner: false }],
          isStarter: 1,
          isWinner: 0,
        },
        {
          questions: [],
          answers: ['yes'],
          board: [{ name: 'Pedro Gonzalez', picture: 'pedrogonzalez.jpg', iswinner: false }],
          isStarter: 0,
          isWinner: 0,
        },
      ]);
    });
    it('should return a JSON of the game state without any changes when the answer is empty', function () {
      assert.strictEqual(answer.add('', game), game);
    });
    it('should return a JSON of the game state with the answer assigned to the correct player if isStarter', function () {
      assert.deepStrictEqual(
        answer.add('yes', [
          {
            questions: ['does she wear glasses?'],
            answers: ['no'],
            board: [{ name: 'Juan Perez', picture: 'juanperez.jpg', iswinner: false }],
            isStarter: 0,
            isWinner: 0,
          },
          {
            questions: ['is he bald?', 'is she blonde?'],
            answers: ['yes'],
            board: [{ name: 'Pedro Gonzalez', picture: 'pedrogonzalez.jpg', iswinner: false }],
            isStarter: 1,
            isWinner: 0,
          },
        ]),
        [
          {
            questions: ['does she wear glasses?'],
            answers: ['no', 'yes'],
            board: [{ name: 'Juan Perez', picture: 'juanperez.jpg', iswinner: false }],
            isStarter: 0,
            isWinner: 0,
          },
          {
            questions: ['is he bald?', 'is she blonde?'],
            answers: ['yes'],
            board: [{ name: 'Pedro Gonzalez', picture: 'pedrogonzalez.jpg', iswinner: false }],
            isStarter: 1,
            isWinner: 0,
          },
        ],
      );
    });
    it('should return a JSON of the game state without changes when there are no questions to answer', function () {
      assert.deepStrictEqual(
        answer.add('yes', [
          {
            questions: ['does she wear glasses?'],
            answers: ['yes'],
            board: [{ name: 'Juan Perez', picture: 'juanperez.jpg', iswinner: false }],
            isStarter: 0,
            isWinner: 0,
          },
          {
            questions: ['is he bald?'],
            answers: ['no'],
            board: [{ name: 'Pedro Gonzalez', picture: 'pedrogonzalez.jpg', iswinner: false }],
            isStarter: 1,
            isWinner: 0,
          },
        ]),
        [
          {
            questions: ['does she wear glasses?'],
            answers: ['yes'],
            board: [{ name: 'Juan Perez', picture: 'juanperez.jpg', iswinner: false }],
            isStarter: 0,
            isWinner: 0,
          },
          {
            questions: ['is he bald?'],
            answers: ['no'],
            board: [{ name: 'Pedro Gonzalez', picture: 'pedrogonzalez.jpg', iswinner: false }],
            isStarter: 1,
            isWinner: 0,
          },
        ],
      );
    });
  });
});
