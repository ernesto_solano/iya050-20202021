const assert = require('chai').assert;
const question = require('../../src/controllers/question.controller.js');

describe('Question', function () {
  describe('#add()', function () {
    var game = [
      {
        questions: [],
        answers: [],
        board: [{ name: 'Juan Perez', picture: 'juanperez.jpg', iswinner: false }],
        isStarter: 0,
        isWinner: 0,
      },
      {
        questions: [],
        answers: [],
        board: [{ name: 'Pedro Gonzalez', picture: 'pedrogonzalez.jpg', iswinner: false }],
        isStarter: 0,
        isWinner: 0,
      },
    ];

    it('should return a JSON of the game state with the question added when the question is valid', function () {
      assert.deepStrictEqual(question.add('is he bald?', game), [
        {
          questions: ['is he bald?'],
          answers: [],
          board: [{ name: 'Juan Perez', picture: 'juanperez.jpg', iswinner: false }],
          isStarter: 1,
          isWinner: 0,
        },
        {
          questions: [],
          answers: [],
          board: [{ name: 'Pedro Gonzalez', picture: 'pedrogonzalez.jpg', iswinner: false }],
          isStarter: 0,
          isWinner: 0,
        },
      ]);
    });
    it('should return a JSON of the game state without any changes when the question is empty', function () {
      assert.strictEqual(question.add('', game), game);
    });
    it('should return a JSON of the game state with the question assigned to the correct player if isStarter', function () {
      assert.deepStrictEqual(
        question.add('is she blonde?', [
          {
            questions: ['does she wear glasses?'],
            answers: ['no'],
            board: [{ name: 'Juan Perez', picture: 'juanperez.jpg', iswinner: false }],
            isStarter: 0,
            isWinner: 0,
          },
          {
            questions: ['is he bald?'],
            answers: ['yes'],
            board: [{ name: 'Pedro Gonzalez', picture: 'pedrogonzalez.jpg', iswinner: false }],
            isStarter: 1,
            isWinner: 0,
          },
        ]),
        [
          {
            questions: ['does she wear glasses?'],
            answers: ['no'],
            board: [{ name: 'Juan Perez', picture: 'juanperez.jpg', iswinner: false }],
            isStarter: 0,
            isWinner: 0,
          },
          {
            questions: ['is he bald?', 'is she blonde?'],
            answers: ['yes'],
            board: [{ name: 'Pedro Gonzalez', picture: 'pedrogonzalez.jpg', iswinner: false }],
            isStarter: 1,
            isWinner: 0,
          },
        ],
      );
    });
    it('should return a JSON of the game state without changes when there are still questions to answer', function () {
      assert.deepStrictEqual(
        question.add('is she blonde?', [
          {
            questions: ['does she wear glasses?'],
            answers: ['yes'],
            board: [{ name: 'Juan Perez', picture: 'juanperez.jpg', iswinner: false }],
            isStarter: 0,
            isWinner: 0,
          },
          {
            questions: ['is he bald?'],
            answers: [],
            board: [{ name: 'Pedro Gonzalez', picture: 'pedrogonzalez.jpg', iswinner: false }],
            isStarter: 1,
            isWinner: 0,
          },
        ]),
        [
          {
            questions: ['does she wear glasses?'],
            answers: ['yes'],
            board: [{ name: 'Juan Perez', picture: 'juanperez.jpg', iswinner: false }],
            isStarter: 0,
            isWinner: 0,
          },
          {
            questions: ['is he bald?'],
            answers: [],
            board: [{ name: 'Pedro Gonzalez', picture: 'pedrogonzalez.jpg', iswinner: false }],
            isStarter: 1,
            isWinner: 0,
          },
        ],
      );
    });
  });
});
