const assert = require('chai').assert;
const guess = require('../../src/controllers/guess.controller.js');

describe('Guess', function () {
  describe('#add()', function () {
    const game = [
      {
        questions: ['is he bald?'],
        answers: [],
        board: [
          { name: 'Juan Perez', picture: 'juanperez.jpg', iswinner: false },
          { name: 'Julio Ramirez', picture: 'julioramirez.jpg', iswinner: true },
        ],
        isStarter: 1,
        isWinner: 0,
      },
      {
        questions: [],
        answers: ['yes'],
        board: [{ name: 'Pedro Gonzalez', picture: 'pedrogonzalez.jpg', iswinner: false }],
        isStarter: 0,
        isWinner: 0,
      },
    ];

    it('should return a JSON of the game state with the winner added when the guess is correct', function () {
      assert.deepStrictEqual(guess.add('Julio Ramirez', game), [
        {
          questions: ['is he bald?'],
          answers: [],
          board: [
            { name: 'Juan Perez', picture: 'juanperez.jpg', iswinner: false },
            { name: 'Julio Ramirez', picture: 'julioramirez.jpg', iswinner: true },
          ],
          isStarter: 1,
          isWinner: 1,
        },
        {
          questions: [],
          answers: ['yes'],
          board: [{ name: 'Pedro Gonzalez', picture: 'pedrogonzalez.jpg', iswinner: false }],
          isStarter: 0,
          isWinner: 0,
        },
      ]);
    });
    it('should return a JSON of the game state with the opposite player added as winner when the guess is incorrect', function () {
      assert.deepStrictEqual(guess.add('Juan Perez', game), [
        {
          questions: ['is he bald?'],
          answers: [],
          board: [
            { name: 'Juan Perez', picture: 'juanperez.jpg', iswinner: false },
            { name: 'Julio Ramirez', picture: 'julioramirez.jpg', iswinner: true },
          ],
          isStarter: 1,
          isWinner: 0,
        },
        {
          questions: [],
          answers: ['yes'],
          board: [{ name: 'Pedro Gonzalez', picture: 'pedrogonzalez.jpg', iswinner: false }],
          isStarter: 0,
          isWinner: 1,
        },
      ]);
    });
    it('should return a JSON of the game state with the opposite player added as winner when the guess is incorrect because the name doesnt exists', function () {
      assert.deepStrictEqual(guess.add('Juanito Perez', game), [
        {
          questions: ['is he bald?'],
          answers: [],
          board: [
            { name: 'Juan Perez', picture: 'juanperez.jpg', iswinner: false },
            { name: 'Julio Ramirez', picture: 'julioramirez.jpg', iswinner: true },
          ],
          isStarter: 1,
          isWinner: 0,
        },
        {
          questions: [],
          answers: ['yes'],
          board: [{ name: 'Pedro Gonzalez', picture: 'pedrogonzalez.jpg', iswinner: false }],
          isStarter: 0,
          isWinner: 1,
        },
      ]);
    });

    it('should return a JSON of the game state without any changes when the guess is empty', function () {
      assert.strictEqual(guess.add('', game), game);
    });
    it('should return a JSON of the game state with the winner added when the guess is correct if is not case sensitive', function () {
      assert.deepStrictEqual(
        guess.add('pedro gonzalez', [
          {
            questions: ['does she wear glasses?'],
            answers: ['no', 'no'],
            board: [{ name: 'Juan Perez', picture: 'juanperez.jpg', iswinner: false }],
            isStarter: 0,
            isWinner: 0,
          },
          {
            questions: ['is he bald?', 'is she blonde?'],
            answers: ['yes'],
            board: [{ name: 'Pedro Gonzalez', picture: 'pedrogonzalez.jpg', iswinner: true }],
            isStarter: 1,
            isWinner: 0,
          },
        ]),
        [
          {
            questions: ['does she wear glasses?'],
            answers: ['no', 'no'],
            board: [{ name: 'Juan Perez', picture: 'juanperez.jpg', iswinner: false }],
            isStarter: 0,
            isWinner: 0,
          },
          {
            questions: ['is he bald?', 'is she blonde?'],
            answers: ['yes'],
            board: [{ name: 'Pedro Gonzalez', picture: 'pedrogonzalez.jpg', iswinner: true }],
            isStarter: 1,
            isWinner: 1,
          },
        ],
      );
    });
    it('should return a JSON of the game state without changes when there are still questions to answer', function () {
      assert.deepStrictEqual(
        guess.add('Pedro Gonzalez', [
          {
            questions: ['does she wear glasses?'],
            answers: ['yes'],
            board: [{ name: 'Juan Perez', picture: 'juanperez.jpg', iswinner: false }],
            isStarter: 0,
            isWinner: 0,
          },
          {
            questions: ['is he bald?'],
            answers: [],
            board: [{ name: 'Pedro Gonzalez', picture: 'pedrogonzalez.jpg', iswinner: true }],
            isStarter: 1,
            isWinner: 0,
          },
        ]),
        [
          {
            questions: ['does she wear glasses?'],
            answers: ['yes'],
            board: [{ name: 'Juan Perez', picture: 'juanperez.jpg', iswinner: false }],
            isStarter: 0,
            isWinner: 0,
          },
          {
            questions: ['is he bald?'],
            answers: [],
            board: [{ name: 'Pedro Gonzalez', picture: 'pedrogonzalez.jpg', iswinner: true }],
            isStarter: 1,
            isWinner: 0,
          },
        ],
      );
    });
  });
});
