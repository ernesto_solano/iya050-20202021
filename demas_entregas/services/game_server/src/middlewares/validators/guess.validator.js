const { body } = require('express-validator');

exports.validate = [body('state').exists(), body('guess').exists()];
