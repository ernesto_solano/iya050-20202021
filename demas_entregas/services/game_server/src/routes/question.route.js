const express = require('express');
const router = express.Router();
const { validationResult } = require('express-validator');
const questionValidator = require('../middlewares/validators/question.validator');
const questionController = require('../controllers/question.controller');
const HttpException = require('../utils/httpexception.constructor');

router.post('/', questionValidator.validate, (req, res, next) => {
  try {
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
      const err = new HttpException(400, 'Bad Request: Malformed Body', errors.array());
      next(err);
    }
    const initialState = JSON.stringify(req.body['state']);
    const state = questionController.add(req.body['question'], req.body['state'], req.body['players-turn']);

    if (JSON.stringify(state) !== initialState) {
      res.status(200).json(state);
    } else {
      const err = new HttpException(400, 'Bad Request: Action Not Allowed', state);
      next(err);
    }
  } catch (err) {
    return next(err);
  }
});
module.exports = router;
