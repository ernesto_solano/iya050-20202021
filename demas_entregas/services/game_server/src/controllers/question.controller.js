const isFirstTurn = (state) => {
  const findPreviousTurn = state.find((player) => player['questions'].length > 0);
  return findPreviousTurn === undefined ? true : false;
};

const whoseTurnIsNext = (state) => {
  const firstPlayerNumberOfQuestions = state[0]['questions'].length;
  const secondPlayerNumberOfQuestions = state[1]['questions'].length;
  const firstPlayerIsStarter = state[0]['isStarter'];
  const isFirstPlayerTurn = 0;
  const isSecondPlayerTurn = 1;
  if (firstPlayerIsStarter === 1) {
    return firstPlayerNumberOfQuestions > secondPlayerNumberOfQuestions ? isSecondPlayerTurn : isFirstPlayerTurn;
  } else {
    return secondPlayerNumberOfQuestions > firstPlayerNumberOfQuestions ? isFirstPlayerTurn : isSecondPlayerTurn;
  }
};

const thereIsQuestionsLeftToAnswer = (state) => {
  const firstPlayerNumberOfQuestions = state[0]['questions'].length;
  const secondPlayerNumberOfQuestions = state[1]['questions'].length;
  const firstPlayerNumberOfAnswers = state[0]['answers'].length;
  const secondPlayerNumberOfAnswers = state[1]['answers'].length;
  return firstPlayerNumberOfQuestions === secondPlayerNumberOfAnswers &&
    secondPlayerNumberOfQuestions === firstPlayerNumberOfAnswers
    ? false
    : true;
};

exports.add = (question, state, playersTurn) => {
  if (thereIsQuestionsLeftToAnswer(state)) return state;

  if (playersTurn === undefined) {
    if (isFirstTurn(state)) {
      playersTurn = 0;
      state[playersTurn].isStarter = 1;
    } else {
      playersTurn = whoseTurnIsNext(state);
    }
  }

  //TODO quitar esta madre puede hacerse con un validator
  if (question === '') return state;
  state[playersTurn]['questions'].push(question);
  return state;
};
