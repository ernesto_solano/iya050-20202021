const whoseTurnIsNextToGuess = (state) => {
  const firstPlayerNumberOfQuestions = state[0]['questions'].length;
  const secondPlayerNumberOfQuestions = state[1]['questions'].length;
  const firstPlayerIsStarter = state[0]['isStarter'];
  const isFirstPlayerTurn = 0;
  const isSecondPlayerTurn = 1;
  if (firstPlayerIsStarter === 1) {
    return firstPlayerNumberOfQuestions > secondPlayerNumberOfQuestions ? isFirstPlayerTurn : isSecondPlayerTurn;
  } else {
    return secondPlayerNumberOfQuestions > firstPlayerNumberOfQuestions ? isSecondPlayerTurn : isFirstPlayerTurn;
  }
};

const thereIsQuestionsLeftToAnswer = (state) => {
  const firstPlayerNumberOfQuestions = state[0]['questions'].length;
  const secondPlayerNumberOfQuestions = state[1]['questions'].length;
  const firstPlayerNumberOfAnswers = state[0]['answers'].length;
  const secondPlayerNumberOfAnswers = state[1]['answers'].length;
  return firstPlayerNumberOfQuestions === secondPlayerNumberOfAnswers &&
    secondPlayerNumberOfQuestions === firstPlayerNumberOfAnswers
    ? false
    : true;
};

const isGuessCorrect = (guess, state, playersTurn) => {
  const namesToLowerCase = state[playersTurn]['board'].map((persons) => {
    return persons.name.toLowerCase();
  });
  const guessToLowerCase = guess.toLowerCase();
  const personGuessed = namesToLowerCase.indexOf(guessToLowerCase);
  const notFound = -1;
  if (personGuessed === notFound) {
    return false;
  }
  return state[playersTurn]['board'][personGuessed]['iswinner'];
};

exports.add = (guess, state) => {
  if (thereIsQuestionsLeftToAnswer(state)) return state;
  if (guess === '') return state;

  const playersTurn = whoseTurnIsNextToGuess(state);

  if (isGuessCorrect(guess, state, playersTurn)) {
    state[playersTurn]['isWinner'] = 1;
  } else {
    if (playersTurn === 0) {
      state[1]['isWinner'] = 1;
      state[0]['isWinner'] = 0;
    } else {
      state[0]['isWinner'] = 1;
      state[1]['isWinner'] = 0;
    }
  }
  return state;
};
