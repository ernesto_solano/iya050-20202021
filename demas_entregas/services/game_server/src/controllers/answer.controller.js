const thereAreUnansweredQuestions = (state) => {
  const firstPlayerNumberOfQuestions = state[0]['questions'].length;
  const secondPlayerNumberOfQuestions = state[1]['questions'].length;
  const firstPlayerNumberOfAsnwers = state[0]['answers'].length;
  const secondPlayerNumberOfAsnwers = state[1]['answers'].length;

  return firstPlayerNumberOfQuestions === secondPlayerNumberOfAsnwers &&
    secondPlayerNumberOfQuestions === firstPlayerNumberOfAsnwers
    ? false
    : true;
};

const whoseTurnIsNext = (state) => {
  const firstPlayerNumberOfQuestions = state[0]['questions'].length;
  const secondPlayerNumberOfQuestions = state[1]['questions'].length;
  const firstPlayerIsStarter = state[0]['isStarter'];
  const isFirstPlayerTurn = 0;
  const isSecondPlayerTurn = 1;

  if (firstPlayerIsStarter) {
    return firstPlayerNumberOfQuestions > secondPlayerNumberOfQuestions ? isSecondPlayerTurn : isFirstPlayerTurn;
  } else {
    return secondPlayerNumberOfQuestions > firstPlayerNumberOfQuestions ? isFirstPlayerTurn : isSecondPlayerTurn;
  }
};

exports.add = (answer, state) => {
  if (!thereAreUnansweredQuestions(state)) return state;
  if (answer === '') return state;
  const playersTurn = whoseTurnIsNext(state);
  state[playersTurn]['answers'].push(answer);
  return state;
};
