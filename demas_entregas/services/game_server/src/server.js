const express = require('express');
const cors = require('cors');
const servererror = require('./middlewares/servererror.middleware');
const HttpException = require('./utils/httpexception.constructor');
const answerRouter = require('./routes/answer.route');
const questionRouter = require('./routes/question.route');
const guessRouter = require('./routes/guess.route');

const app = express();

app.use(express.json());
app.use(cors());
app.options('*', cors());

app.get('/', (req, res) =>
  res.send(
    '<center><H3>Guess Who? API </H3> <br> educational purposes <br><br></center> <b>Endpoints</b> <ul> <li> <b> POST</b> api/v1/question</li> <li> <b> POST</b> api/v1/answer</li> <li> <b> POST</b> api/v1/guess</li> </ul>',
  ),
);
app.use(`/api/v1/answer`, answerRouter);
app.use(`/api/v1/question`, questionRouter);
app.use(`/api/v1/guess`, guessRouter);

// 404 error
app.all('*', (req, res, next) => {
  const err = new HttpException(404, 'Endpoint Not Found');
  next(err);
});

// Error middleware
app.use(servererror);

const port = process.env.PORT || 3000;
app.listen(port, () => console.log(`🚀 Server running on port ${port}!`));

module.exports = app;
