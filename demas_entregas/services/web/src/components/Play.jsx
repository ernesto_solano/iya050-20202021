import React, { useEffect, useState } from "react";


const Play = () => {
  const [data, setData] = useState(null);
  const [hover1, sethover1] = useState(false);
  const [hover2, sethover2] = useState(false);
  const [question, setquestion] = useState("");
  const [answer, setAnswer] = useState("");
  const [guess, setGuess] = useState("");

  const sendQuestion = () => {
    fetch(`http://localhost:8081/game/${localStorage.getItem("idGame")}/question`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({ "question": question })
    }).then(response => response.json()).then(json => {
      setData(json);
    });
  }
  const sendAnswer = () => {
    fetch(`http://localhost:8081/game/${localStorage.getItem("idGame")}/answer`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({ "answer": answer })
    }).then(response => response.json()).then(json => {
      setData(json);
    });
  }
  const sendGuess = () => {
    fetch(`http://localhost:8081/game/${localStorage.getItem("idGame")}/guess`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({ "guess": guess })
    }).then(response => response.json()).then(json => {
      setData(json);
    });
  }

  const newGame = () => {
    localStorage.removeItem("idGame");
    fetch(`http://localhost:8081/game`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      }
    }).then(response => response.json()).then(json => {
      setData(json);
      localStorage.setItem("idGame", json.id)
    });
  }

  const handleMouseHover1 = () => {
    sethover1(!hover1);
  }

  const handleMouseHover2 = () => {
    sethover2(!hover2);
  }

  useEffect(() => {
    if (!localStorage.getItem("idGame")) {
      fetch(`http://localhost:8081/game`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        }
      }).then(response => response.json()).then(json => {
        setData(json);
        localStorage.setItem("idGame", json.id)
      });
    }
    else {
      fetch(`http://localhost:8081/game/${localStorage.getItem("idGame")}`, {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json'
        }
      }).then(response => response.json()).then(json => {
        setData(json);
      });
    }

  }, []);

  return (
    <div style={{ textAlign: "center", color: "#F5EDE3", fontFamily: "sans-serif", fontSize: "1.25rem", textShadow: "0 1px 1px #b6701e" }}>
      <div style={{ display: "flex", backgroundColor: "#242729", padding: "1rem" }}>
        <div style={{ flex: 1, display: "flex" }}>
          <input type="text" onChange={(event) => setquestion(event.target.value)} /> <button onClick={sendQuestion}>question</button>
        </div>
        <div style={{ flex: 1, display: "flex" }}>
          <input type="text" onChange={(event) => setAnswer(event.target.value)} /> <button onClick={sendAnswer}>answer</button>
        </div>
        <div style={{ flex: 1, display: "flex" }}>
          <input type="text" onChange={(event) => setGuess(event.target.value)} /> <button onClick={sendGuess}>guess</button>
        </div>
        {
          (data !== null && (data.game.state[0].isWinner === 1 || data.game.state[1].isWinner === 1)) &&
          <div style={{ display: "flex" }}>
            <button onClick={newGame}>new game</button>
          </div>
        }
      </div>
      <div style={{ display: "flex", textAlign: "center", color: "#F5EDE3", fontFamily: "sans-serif", fontSize: "1.25rem", textShadow: "0 1px 1px #b6701e" }}>
        <div style={{ flex: 1, padding: "1rem", display: "flex", flexDirection: "column", backgroundColor: "#0141CF" }}>
          <h2>Player 1 {(data !== null && data.game.state[0].isWinner === 1) && <span>WINS</span>}</h2>
          <div style={{ display: "inline-block" }}>
            {data !== null && data.game.state[0].board.map((e, k) => {
              return <div style={{ display: "inline-block", margin: "1rem" }}>  <svg width="100" height="100">
                <rect width="100" height="100" />
              </svg> <p key={k}> {e.name} </p></div>
            })}
          </div>
          <h3>Character Description</h3>
          {data !== null && data.game.state[0].questions.map((e, k) => {
            return <div> <p key={k}> {e} <span style={{ fontWeight: "bold" }}>{data.game.state[1].answers[k]}</span> </p></div>
          })}
          <div style={{ minHeight: "300px" }}>

            <div onMouseEnter={handleMouseHover1} onMouseLeave={handleMouseHover1}> <h3>Show your character</h3>{hover1 &&
              <div >{data !== null && data.game.state[1].board.filter((e) => e.iswinner === true).map((e, k) => {
                return <div style={{ display: "inline-block", margin: "1rem" }}>  <svg width="100" height="100">
                  <rect width="100" height="100" />
                </svg> <p key={k}> {e.name} </p></div>
              })}</div>}
            </div>
          </div>
        </div>
        <div style={{ flex: 1, padding: "1rem", display: "flex", flexDirection: "column", backgroundColor: "#FD7F00" }}>
          <h2>Player 2 {(data !== null && data.game.state[1].isWinner === 1) && <span>WINS</span>}</h2>
          <div style={{ display: "inline-block" }}>
            {data !== null && data.game.state[1].board.map((e, k) => {
              return <div style={{ display: "inline-block", margin: "1rem" }}>  <svg width="100" height="100">
                <rect width="100" height="100" />
              </svg> <p key={k}> {e.name} </p></div>
            })}
          </div>
          <h3>Character Description</h3>
          {data !== null && data.game.state[1].questions.map((e, k) => {
            return <div> <p key={k}> {e} <span style={{ fontWeight: "bold" }}>{data.game.state[0].answers[k]}</span> </p></div>
          })}
          <div style={{ minHeight: "300px" }}>

            <div onMouseEnter={handleMouseHover2} onMouseLeave={handleMouseHover2} > <h3>Show your character</h3>{hover2 &&
              <div >{data !== null && data.game.state[0].board.filter((e) => e.iswinner === true).map((e, k) => {
                return <div style={{ display: "inline-block", margin: "1rem" }}>  <svg width="100" height="100">
                  <rect width="100" height="100" />
                </svg> <p key={k}> {e.name} </p></div>
              })}</div>}
            </div>
          </div>
        </div>
      </div>
    </div>);
};

export default Play;
