import React, { useState, useContext } from "react";
import { Redirect, useHistory } from "react-router-dom";
import PropTypes from "prop-types";
import TextInput from "./TextInput.jsx";
import AuthContext from "../AuthContext.js";

const SignIn = ({ returnTo }) => {

  const [formValues, setFormValues] = useState({ name: "" });
  const history = useHistory();
  /* if (!localStorage.getItem("logedUser")) return <Redirect to="/sign-in" />; */

  const handleInputChange = (event) => {
    const target = event.target;
    setFormValues({ ...formValues, [target.name]: target.value });
  };

  const handleSubmit = (event) => {
    event.preventDefault();
    localStorage.setItem("logedUser", formValues.name);
    history.push("/game")
  };

  return (
    <form onSubmit={handleSubmit}>
      <TextInput
        label="Name"
        name="name"
        value={formValues.name}
        onChange={handleInputChange}
      />
      <input type="submit" />
    </form>
  );
};
SignIn.propTypes = {
  returnTo: PropTypes.string.isRequired,
};

export default SignIn;
