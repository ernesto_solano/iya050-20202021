const Koa = require("koa");
const cors = require("@koa/cors");
const koaBody = require("koa-body");
const Router = require("@koa/router");
const fs = require('fs');
const path = require('path');
const fetch = require('node-fetch');

const app = new Koa();
const router = new Router();

router.post("/game", (ctx) => {
  const id = new Date().getTime();
  const game = {
    "state": [{
      "questions": [],
      "answers": [],
      "board": [{ "name": "Juan Perez", "picture": "juanperez.jpg", "iswinner": true }, { "name": "Maria Lopez", "picture": "marialopez.jpg", "iswinner": false }],
      "isStarter": 0,
      "isWinner": 0
    },
    {
      "questions": [],
      "answers": [],
      "board": [{ "name": "Pedro Gonzalez", "picture": "pedrogonzalez.jpg", "iswinner": false }, { "name": "Julio Gomez", "picture": "juliogomez.jpg", "iswinner": true }],
      "isStarter": 0,
      "isWinner": 0
    }],
  }

  fs.writeFile(`${id}.json`, JSON.stringify(game), 'utf8', (err) => {
    if (err) throw err;
    console.log("creado");
  });
  // 1. post mutation to Stats server to store new game
  // 2. return state

  ctx.response.set("Content-Type", "application/json");
  ctx.body = JSON.stringify({
    id: id,
    game: game
  });
});


router.get("/game/:id", (ctx) => {
  // 1. get current state from Stats server
  // 2. return state

  const data = fs.readFileSync(`${ctx.params.id}.json`, 'utf8');
  ctx.response.set("Content-Type", "application/json");
  ctx.body = JSON.stringify({
    id: ctx.params.id,
    game: JSON.parse(data)
  });

});

async function postData(url = '', data = {}) {
  const response = await fetch(url, {
    method: 'POST',
    mode: 'cors',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(data)
  });
  return response.json();
}

router.post("/game/:id/question", async (ctx) => {

  const dataReaded = fs.readFileSync(`${ctx.params.id}.json`, 'utf8');
  // 1. get current state from Stats server
  // 2. get next state from Game server
  // 3. post mutation to Stats server to store next state
  // 4. return state
  const body = ctx.request.body;
  //const question = body["question"];

  const postBody = { "state": JSON.parse(dataReaded).state, "question": body.question }
  await postData(`${process.env.GAME_SERVER}/question`, postBody).then(data => {
    if (data.status === 400 || data.status === 500) {
      console.error("error while posting question to game server")
      ctx.response.set("Content-Type", "application/json");
      ctx.body = JSON.stringify({
        id: ctx.params.id,
        game: JSON.parse(dataReaded)
      });
    }
    else {
      fs.writeFile(`${ctx.params.id}.json`, JSON.stringify({ "state": data }), 'utf8', (err) => {
        if (err) throw err;
        console.log("creado");
      });
      ctx.response.set("Content-Type", "application/json");
      ctx.body = JSON.stringify({
        id: ctx.params.id,
        game: { state: data }
      });
    }

  })
});

router.post("/game/:id/answer", async (ctx) => {

  const dataReaded = fs.readFileSync(`${ctx.params.id}.json`, 'utf8');
  const body = ctx.request.body;

  const postBody = { "state": JSON.parse(dataReaded).state, "answer": body.answer }
  console.log(postBody);
  await postData(`${process.env.GAME_SERVER}/answer`, postBody).then(data => {
    if (data.status === 400) {
      console.error("error while posting answer to game server", ctx.request.body, data)
      ctx.response.set("Content-Type", "application/json");
      ctx.body = JSON.stringify({
        id: ctx.params.id,
        game: JSON.parse(dataReaded)
      });
    }
    else {
      fs.writeFile(`${ctx.params.id}.json`, JSON.stringify({ "state": data }), 'utf8', (err) => {
        if (err) throw err;
        console.log("creado");
      });
      ctx.response.set("Content-Type", "application/json");
      ctx.body = JSON.stringify({
        id: ctx.params.id,
        game: { state: data }
      });
    }
  })
});

router.post("/game/:id/guess", async (ctx) => {

  const dataReaded = fs.readFileSync(`${ctx.params.id}.json`, 'utf8');
  const body = ctx.request.body;

  const postBody = { "state": JSON.parse(dataReaded).state, "guess": body.guess }
  console.log(postBody);
  await postData(`${process.env.GAME_SERVER}/guess`, postBody).then(data => {
    if (data.status === 400) {
      console.error("error while posting answer to game server", ctx.request.body, data)
      ctx.response.set("Content-Type", "application/json");
      ctx.body = JSON.stringify({
        id: ctx.params.id,
        game: JSON.parse(dataReaded)
      });
    }
    else {
      fs.writeFile(`${ctx.params.id}.json`, JSON.stringify({ "state": data }), 'utf8', (err) => {
        if (err) throw err;
        console.log("creado");
      });
      ctx.response.set("Content-Type", "application/json");
      ctx.body = JSON.stringify({
        id: ctx.params.id,
        game: { state: data }
      });
    }
  })
});



app.use(koaBody());
app.use(cors());
app.use(router.routes());
app.use(router.allowedMethods());

app.listen(process.env.PORT);
