const path = require('path');

const HtmlWebpackPlugin = require('html-webpack-plugin');


module.exports = {


  entry: {

    app: './src/index.js',

  },

  
  plugins: [
    new HtmlWebpackPlugin({
      title: 'Entrega 1',
      template: path.resolve(__dirname, 'app/index.html'),
      filename: 'index.html',
      inject: 'body',
      scriptLoading: 'blocking'
    }),
  ],

  output: {

    filename: '[name].bundle.js',

    path: path.resolve(__dirname, 'dist'),

    clean: true,

  },

  module: {
    rules: [
      {
        test: /\.m?js$/,
        exclude: /(node_modules|bower_components)/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['@babel/preset-env']
          }
        }
      }
    ]
  }



};