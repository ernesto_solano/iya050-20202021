async function postData(url = '', data = {}) {
    // Default options are marked with *
    const response = await fetch(url, {
        method: 'POST', // *GET, POST, PUT, DELETE, etc.
        headers: {
            'Content-Type': 'application/json'
            // 'Content-Type': 'application/x-www-form-urlencoded',
        },
        body: JSON.stringify(data) // body data type must match "Content-Type" header
    });
    console.log(response)
    return response;
}

async function getData(url = '', headers={}) {
    // Default options are marked with *
    const response = await fetch(url, {
        method: 'GET', // *GET, POST, PUT, DELETE, etc.
        headers: {
            'Content-Type': 'application/json',
            ...headers
            // 'Content-Type': 'application/x-www-form-urlencoded',
        }
    });
    console.log(response)
    return response;
}

async function deleteData(url = '',headers={}) {
    // Default options are marked with *
    const response = await fetch(url, {
        method: 'DELETE', // *GET, POST, PUT, DELETE, etc.
        headers: {
            'Content-Type': 'application/json',
            ...headers
            // 'Content-Type': 'application/x-www-form-urlencoded',
        }
    });
    console.log(response)
    return response;
}


export { postData, getData, deleteData };