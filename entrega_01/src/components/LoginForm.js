import React from 'react';
import { postData } from '../utils/http-request';
import { Redirect } from "react-router-dom";


function Greeting(props) {
  return <h1>Log in</h1>;
}

function Redirected(props) {
  return (<Redirect to="/home" />)
}

class LoginForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = { username: '', password: '', success: false };
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  async login() {

    try {
      const response = await postData(
        `https://6fra5t373m.execute-api.eu-west-1.amazonaws.com/development/users/${this.state.username}/sessions`,
        {
          "password": this.state.password
        }
      );
      if ([201, 200].indexOf(response.status) !== -1) {
        const responsejson = await response.json()
        console.log("me voy a logear", responsejson);
        sessionStorage.setItem('username', this.state.username);
        sessionStorage.setItem('credential', responsejson["sessionToken"]);
        this.setState({ success: true })
      }
      else {
        console.log("TODO FALLO")
      }
    } catch (error) {
      console.log("fallo", error)
    }

  }

  handleChange(event) {
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;

    this.setState({
      [name]: value
    });
  }
  handleSubmit(event) {
    event.preventDefault();
    this.login();
  }

  render() {
    return (
      <div>
        <Greeting />
        <form onSubmit={this.handleSubmit}>
          <label>
            Username:
          <input name="username" type="text" value={this.state.username} onChange={this.handleChange} />
          </label>
          <label>
            Password:
          <input name="password" type="password" value={this.state.password} onChange={this.handleChange} />
          </label>
          <input type="submit" value="Log in" />
        </form>
        {this.state.success &&
          <Redirected />
        }
      </div>

    );
  }
}
export default LoginForm
