import React from 'react';
import { Redirect } from "react-router-dom";
import { getData, deleteData } from '../utils/http-request';


function Redirected(props) {
    if (sessionStorage.getItem('credential') == null) {

        return (<Redirect to="/login" />);
    }
    else {
        return (<a> </a>)
    }
}

class Home extends React.Component {
    constructor(props) {
        super(props);
        this.state = { user: null, credentials: '' };
    }

    async logout() {
        const credentials = sessionStorage.getItem('credential');
        const username = sessionStorage.getItem('username');

        await deleteData(`https://6fra5t373m.execute-api.eu-west-1.amazonaws.com/development/users/${username}/sessions/${credentials}`,
            { 'authorization': credentials });
        sessionStorage.clear();
        this.forceUpdate();
    }

    async load() {
        const credentials = sessionStorage.getItem('credential');
        const user = await getData('https://6fra5t373m.execute-api.eu-west-1.amazonaws.com/development/user',
            { 'authorization': credentials });
        const responsejson = await user.json();
        console.log(credentials);
        if (credentials === null) { //prevent infinite loop 
            return;
        }
        if (typeof (responsejson.firstName) == 'undefined') {
            this.load();
        }
        this.setState({ user: responsejson, credentials: credentials });
        console.log(responsejson);
        console.log('1')
    }

    async componentDidMount() {
        await this.load();
        console.log('2')
    }
    render() {

        return (
            <div>
                {this.state.user != null && <h3> Home {this.state.user.firstName}</h3>}
                <button onClick={async () => { await this.logout() }}>log out</button>

                <Redirected />
            </div>

        );
    }
}
export default Home
