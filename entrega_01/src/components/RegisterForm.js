import React from 'react';
import { postData } from '../utils/http-request';
import { Redirect } from "react-router-dom";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
} from "react-router-dom";
function Greeting(props) {
  return <h1>Register</h1>;
}

function Redirected(props) {
  return (<Redirect to="/login" />)
}

class RegisterForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = { username: '', password: '', firstname: '', success: false };
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  async register() {
    try {
      const response = await postData(
        `https://6fra5t373m.execute-api.eu-west-1.amazonaws.com/development/users/${this.state.username}`,
        {
          "firstName": this.state.firstname,
          "password": this.state.password
        }
      );
      if ([201, 200].indexOf(response.status) !== -1) {
        console.log("me voy a registrar", await response.json());
        this.setState({ success: true })
      }
      else {
        console.log("Error al registrarse")
      }
    } catch (error) {
      console.log("fallo", error)
    }
  }

  handleChange(event) {
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;

    this.setState({
      [name]: value
    });
  }
  handleSubmit(event) {
    event.preventDefault();
    this.register();
  }

  render() {
    return (
      <div>
        <Greeting />
        <form onSubmit={this.handleSubmit}>
          <label>
            Username:
          <input name="username" type="text" value={this.state.username} onChange={this.handleChange} />
          </label>
          <label>
            First Name:
          <input name="firstname" type="text" value={this.state.firstname} onChange={this.handleChange} />
          </label>
          <label>
            Password:
          <input name="password" type="password" value={this.state.password} onChange={this.handleChange} />
          </label>
          <input type="submit" value={this.props.isLogin ? "Log in" : "Register"} />
        </form>
        {this.state.success &&
          <Redirected />
        }
        <Link to="/login">Already have an account?</Link>
      </div>

    );
  }
}
export default RegisterForm
